<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function index()
	{
		$header['page_title'] = 'beranda';

		$this->load->view('templates/header', $header);
		$this->load->view('beranda');
		$this->load->view('templates/footer');
	}
}

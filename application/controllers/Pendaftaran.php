<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

	public function index()
	{
		$header['page_title'] = 'pendaftaran';

		$this->load->view('templates/header', $header);
		$this->load->view('pendaftaran');
		$this->load->view('templates/footer');
	}

	public function submit(){
		$inputpost = $this->input->post();
		$data = $inputpost;
		$password = random_string('alnum', 8);
		$data['password_pendaftar'] = md5($password);

		if($this->db->insert('user_pendaftar', $data)){
			if($this->send_email($data,$password) == 1){
				echo '<div class="alert alert-success" id="notif-alert" role="alert">Pendaftaran telah berhasil ! Password telah dikirimkan ke email <strong>'.$data['email_pendaftar'].'</strong></div>';
			}else{
				echo '<div class="alert alert-success" id="notif-alert" role="alert">Pendaftaran telah berhasil ! Password telah dikirimkan ke email <strong>'.$data['email_pendaftar'].'</strong></div>';
			}
		}
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';

	}

	public function email(){
		$data['peserta'] = array(
			'nama_pendaftar' => 'Triono Nugroho',
			'nisn_pendaftar' => '0123456789',
			'no_kip_pendaftar' => '0123456789',
			'kontak_pendaftar' => '081381705594',
			'alamat_pendaftar' => 'Jl. Delima V Gg 7',
			'email_pendaftar' => 'onocoolbgt@gmail.com',
			'password_pendaftar' => '123456'
		);
		$data['password'] = array('password_pendaftar' => '123456');
		$this->load->view('mail/akun',$data);
	}

    public function send_email($data,$password){
        $data_peserta['peserta'] = $data;
        $data_peserta['password'] = array('password_pendaftar' => $password);
        $pesan = $this->load->view('mail/akun', $data_peserta ,true);
        $this->load->library('Mailing');
        $subject = 'Informasi Akun ARKI';
        return $this->mailing->send_email($data['email_pendaftar'], $pesan, $subject);
    }
}

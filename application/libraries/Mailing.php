<?php

class Mailing {


    function send_email($penerima,$pesan,$subject){
        $CI =& get_instance();

        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'arkiofficial.id@gmail.com';
        $config['smtp_pass']    = 'arki123456';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html

        $CI->load->library('email', $config);
        $CI->email->initialize($config);

        $CI->email->from('arkiofficial.id@gmail.com', 'ARKI KEMENDIKBUD');
        $CI->email->to($penerima); 

        $CI->email->subject($subject);
        $CI->email->message($pesan); 
        
        if (!$CI->email->send()) {
            // Raise error message
            // show_error($CI->email->print_debugger());
            return 0;
        } else {
            // Show success notification or other things here
            // echo 'Success to send email';
            return 1;
        }
    }

	function send_email_smtp($sent_to,$pesan,$subject,$file_invoice = "",$file_bukti_sewa = ""){
        $CI =& get_instance();

        $config['protocol']    = 'smtp';
        $config['smtp_host']    = '10.254.250.26';
        $config['smtp_port']    = '25';
        // $config['smtp_timeout'] = '7';
        $config['smtp_user']    = '';
        $config['smtp_pass']    = '';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $CI->load->library('email', $config);
        $CI->email->initialize($config);

        $CI->email->from('pib.parepare@bulog.co.id', 'Pasar Induk Beras Parepare');
        $CI->email->to($sent_to); 

        $CI->email->set_crlf( "\r\n" );

        $CI->email->subject($subject);
        $CI->email->message($pesan);

        if (isset($file_invoice) && $file_invoice != '') {
            $CI->load->helper('path');
            $path = set_realpath('./assets/pdf/');
            $CI->email->attach($path.'Invoice_'.$file_invoice.'.pdf');
        }

        if (isset($file_bukti_sewa) && $file_bukti_sewa != '') {
            $CI->load->helper('path');
            $path = set_realpath('./assets/pdf/');
            $CI->email->attach($path.'Buktisewa_'.$file_bukti_sewa.'.pdf');
        }
        
        if (!$CI->email->send()) {
            // Raise error message
            // echo show_error($CI->email->print_debugger());
            // log_message('error', 'Kirim Email Gagal ke '.$sent_to.' '.$subject);
            // exit();
            return 0;
        } else {
            // Show success notification or other things here
            // echo 'Success to send email';
            return 1;
        }
    }

}
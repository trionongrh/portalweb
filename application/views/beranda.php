   <div class="tw-slider-area owl-carousel">
      <div class="slider-items">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6">
                  <div class="slider-content">
                     <h1> Digital <span>Marketing</span> Drives Result </h1>
                     <p class="slider-desc">Start working with an company that provide every thing you need to generate awareness, drive traffic,
                        connect with customers, and increase.</p>
                     <a href="#" class="btn btn-primary">Free Analysis</a>
                  </div>
                  <!-- Slider Content End -->
               </div>
               <!-- Col End -->
               <div class="col-md-6">
                  <img src="<?php echo base_url('assets/images/slider/slider1.png');?>" alt="" class="img-fluid">
               </div>
            </div>
            <!-- Row ENd -->
         </div>
         <!-- Container End -->
      </div>
      <!-- 1st Slider -->
      <div class="slider-items bg-orange">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6">
                  <img src="<?php echo base_url('assets/images/slider/slider2.png');?>" alt="" class="img-fluid">
               </div>
               <div class="col-md-6">
                  <div class="slider-content slide-text-right">
                     <h1> Online <span>Marketing</span> Solutions </h1>
                     <p class="slider-desc">Start working with an company that provide every thing you need to generate awareness, drive traffic,
                        connect with customers, and increase.</p>
                     <a href="#" class="btn btn-primary">Free Analysis</a>
                  </div>
                  <!-- Slider Content End -->
               </div>
               <!-- Col End -->
            </div>
            <!-- Row ENd -->
         </div>
         <!-- Container End -->
      </div>
      <!-- 1st Slider -->
   </div>
   <!-- Slider Area End -->

   <section id="tw-features" class="tw-features-area">
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading wow fadeInDown">
               <h2>
                  <small>Our Features</small>
                  Our Best
                  <span>Features</span>
               </h2>
            </div>
            <!-- Title Col End -->
         </div>
         <!-- Title Row End -->
         <div class="row">
            <div class="col-lg-4 col-md-12 text-md-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/feature1.png');?>" alt="">
                     </div>
                     <!-- End Features icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>Quality Service Provide</h3>
                  <p>Internet marketing SEO company offering innovativ marketing</p>
                  <a href="#" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
            <!-- Col End -->
            <div class="col-lg-4 col-md-12  wow fadeInUp" data-wow-duration="1.6s" data-wow-delay=".4s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/feature2.png');?>" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>Professional Marketing </h3>
                  <p>Internet marketing SEO company offering innovativ marketing</p>
                  <a href="#" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
            <!-- end col -->
            <div class="col-lg-4 col-md-12  wow fadeInUp" data-wow-duration="1.9s" data-wow-delay=".6s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/feature3.png');?>" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>More Traffic &amp; Sales</h3>
                  <p>Internet marketing SEO company offering innovativ marketing</p>
                  <a href="#" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
            <!-- End col -->
         </div>
         <!-- End Row 2 -->
      </div>
      <!-- End Container -->
   </section>
   <!-- End Features section -->

   <section id="tw-facts" class="tw-facts bg-overlay">
      <div class="container">
         <div class="row">
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-content wow fadeInUp" data-wow-duration="1s">
                     <span class="counter">200</span>
                     <sup>+</sup>
                     <h4 class="facts-title">Active clients</h4>
                  </div>
                  <!-- Facts Content End -->
               </div>
               <!-- Facts Box End -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-content wow slideInUp">
                     <span class="counter">570</span>
                     <sup>+</sup>
                     <h4 class="facts-title">Projects Done</h4>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-content wow slideInUp">
                     <span class="counter">98</span>
                     <sup>%</sup>
                     <h4 class="facts-title">Success Rate</h4>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-content wow slideInUp">
                     <span class="counter">50</span>
                     <sup>+</sup>
                     <h4 class="facts-title">Awards</h4>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Facts End -->

   <section id="tw-intro" class="tw-intro-area">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 col-md-12 wow fadeInRight" data-wow-duration="1s">
               <h2 class="column-title">We Solve <span>Online Marketing</span> Problems</h2>
               <p>
                  Internet marketing SEO company offering innovati marketing solution to mid to large size companies across the globe. As a
                  leader in SEO, web design. ecommerce, website conversion.
               </p>
               <ul class="intro-list">
                  <li>Internet marketing SEO company offering innovative designs</li>
                  <li>Marketing generate awareness, drive traffic, connect with</li>
               </ul>
               <a href="#" class="btn btn-primary">learn more</a>
               <!-- Default Round Btn -->
               <a href="#" class="btn btn-secondary">contact us</a>
            </div>
            <!-- End Col -->
            <div class="col-lg-6 col-md-12 text-lg-right text-md-center wow fadeInLeft" data-wow-duration="1s">
               <img src="<?php echo base_url('assets/images/about/about_image.png');?>" alt="" class="img-fluid">
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
   <!-- Intro section End -->

   <section id="tw-service" class="tw-service bg-lightgray">
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading wow fadeInDown">
               <h2>
                  <small>Our features</small>
                  Our Best <span>Services</span>
               </h2>
            </div>
            <!-- Title Col End -->
         </div>
         <!-- Title Row End -->
         <div class="row">
            <div class="col-md-4">
               <div class="tw-service-box wow zoomIn" data-wow-duration="1s" data-wow-delay=".2s">
                  <div class="service-icon service-icon-bg-1 d-table">
                     <div class="service-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/service1.png');?>" alt="search engine" class="img-fluid">
                     </div>
                  </div>
                  <!-- Service icon end -->
                  <div class="service-content">
                     <h3>Search Engine Optimization</h3>
                     <p>Internet marketing company offering innovativ marketing solutions to mid to large size companies.</p>
                  </div>
                  <!-- Service Content end -->
               </div>
               <!-- Service box end -->
            </div>
            <!-- Col End -->
            <div class="col-md-4">
               <div class="tw-service-box wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                  <div class="service-icon service-icon-bg-2 d-table">
                     <div class="service-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/service2.png');?>" alt="search engine" class="img-fluid">
                     </div>
                  </div>
                  <!-- Service icon end -->
                  <div class="service-content">
                     <h3>Content Marketing</h3>
                     <p>One of the top 100 advertising and marketing agencies knows what it means to be</p>
                  </div>
                  <!-- Service content end -->
               </div>
               <!-- Service box End -->
            </div>
            <!-- Col End -->
            <div class="col-md-4">
               <div class="tw-service-box wow zoomIn" data-wow-duration="1s" data-wow-delay=".6s">
                  <div class="service-icon service-icon-bg-3 d-table">
                     <div class="service-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/service3.png');?>" alt="search engine" class="img-fluid">
                     </div>
                  </div>
                  <!-- Service Icon End -->
                  <div class="service-content">
                     <h3>Data Analysis</h3>
                     <p>One of the top 100 advertising and marketing agencies knows what it means to be</p>
                  </div>
                  <!-- Service content end -->
               </div>
               <!-- Service box End -->
            </div>
            <!-- Col end -->
         </div>
         <!-- Row end -->
         <div class="tw-mb-30"></div>
         <div class="row">
            <div class="col-md-4">
               <div class="tw-service-box wow zoomIn" data-wow-duration="1s" data-wow-delay=".2s">
                  <div class="service-icon service-icon-bg-4 d-table">
                     <div class="service-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/service4.png');?>" alt="search engine" class="img-fluid">
                     </div>
                  </div>
                  <!-- Service icon end -->
                  <div class="service-content">
                     <h3>Digital Marketing</h3>
                     <p>One of the top 100 advertising and marketing agencies knows what it means to be</p>
                  </div>
                  <!-- Service content end -->
               </div>
               <!-- Service Box end -->
            </div>
            <!-- Col end -->
            <div class="col-md-4">
               <div class="tw-service-box wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                  <div class="service-icon service-icon-bg-5 d-table">
                     <div class="service-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/service5.png');?>" alt="search engine" class="img-fluid">
                     </div>
                  </div>
                  <!-- Service icon end -->
                  <div class="service-content">
                     <h3>Web Analytics</h3>
                     <p>One of the top 100 advertising and marketing agencies knows what it means to be</p>
                  </div>
                  <!-- Service content end -->
               </div>
               <!-- Service box end -->
            </div>
            <!-- Row End -->
            <div class="col-md-4">
               <div class="tw-service-box wow zoomIn" data-wow-duration="1s" data-wow-delay=".6s">
                  <div class="service-icon service-icon-bg-6 d-table">
                     <div class="service-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/service6.png');?>" alt="search engine" class="img-fluid">
                     </div>
                  </div>
                  <!-- Service Icon end-->
                  <div class="service-content">
                     <h3>Social Marketing</h3>
                     <p>One of the top 100 advertising and marketing agencies knows what it means to be</p>
                  </div>
                  <!-- Service Content end -->
               </div>
               <!-- Service box end-->
            </div>
            <!-- Col end -->
         </div>
         <!-- Row End -->
      </div>
      <!-- container -->
   </section>
   <!-- Tw Service End -->

   <section id="work-process" class="work-process bg-green">
      <div class="work-bg-pattern d-none d-lg-inline-block">
         <img src="<?php echo base_url('assets/images/process/process_arrow.png');?>" alt="" class="img-fluid wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.2s">
      </div>
      <!-- End Work BG Pattern -->
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading tw-text-white wow fadeInDown" data-wow-duration="1s">
               <h2>
                  <small>Our Process</small>Our Working <span>Process</span></h2>
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
         <div class="row">
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".2s">
                     <div class="process-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/process1.png');?>" alt="" class="img-fluid">
                        <span class="case-process-number">1</span>
                     </div>
                  </div>
                  <!-- End process wrapper -->
                  <p>Research Project</p>
               </div>
               <!-- End Tw work process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                     <div class="process-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/process2.png');?>" alt="" class="img-fluid">
                        <span class="case-process-number">2</span>
                     </div>
                  </div>
                  <!-- End Process Wrapper -->
                  <p>Find Ideas</p>
               </div>
               <!-- End Word Process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".6s">
                     <div class="process-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/process3.png');?>" alt="" class="img-fluid">
                        <span class="case-process-number">3</span>
                     </div>
                  </div>
                  <!-- End Process Wrapper -->
                  <p>Start Optimize</p>
               </div>
               <!-- End Work Process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".8s">
                     <div class="process-inner d-table-cell">
                        <img src="<?php echo base_url('assets/images/icon/process4.png');?>" alt="" class="img-fluid">
                        <span class="case-process-number">4</span>
                     </div>
                  </div>
                  <!-- End PRocess Wrapper -->
                  <p>Reach Target</p>
               </div>
               <!-- End Work Process -->
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
   <!-- End Word Process -->

   <section id="tw-case" class="tw-case">
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading wow fadeInDown" data-wow-deuration="1s" data-wow-delay=".2s">
               <h2>
                  <small>Our Cases</small>
                  Our Case <span>Studies</span>
               </h2>
               <!-- End Section Title -->
               <span class="animate-border border-offwhite ml-auto mr-auto tw-mt-20"></span>
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
         <div class="row">
            <div class="col-lg-4 col-md-12 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
               <div class="tw-case-study-box">
                  <div class="case-img study-bg-1">
                     <img src="<?php echo base_url('assets/images/cases/case-study1.png');?>" alt="" class="img-fluid">
                  </div>
                  <!-- End case img -->
                  <div class="casestudy-content">
                     <a href="#">
                        <h4>Web Traffic Management</h4>
                     </a>
                     <p>SEO, Marketing</p>
                  </div>
                  <!-- End case study content -->
               </div>
               <!-- End case study box -->
            </div>
            <!-- End Col -->
            <div class="col-lg-4 col-md-12 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
               <div class="tw-case-study-box">
                  <div class="case-img study-bg-2">
                     <img src="<?php echo base_url('assets/images/cases/case-study2.png');?>" alt="" class="img-fluid">
                  </div>
                  <div class="casestudy-content">
                     <a href="#">
                        <h4>Cloaking &amp; Doorway Pages</h4>
                     </a>
                     <p>Social, SEO</p>
                  </div>
                  <!-- End case study content -->
               </div>
               <!-- End case study box -->
            </div>
            <!-- End Col -->
            <div class="col-lg-4 col-md-12 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
               <div class="tw-case-study-box">
                  <div class="case-img study-bg-3">
                     <img src="<?php echo base_url('assets/images/cases/case-study3.png');?>" alt="" class="img-fluid">
                  </div>
                  <div class="casestudy-content">
                     <a href="#">
                        <h4>Hosting company rank</h4>
                     </a>
                     <p>Hosting, Marketing</p>
                  </div>
                  <!-- End case study content -->
               </div>
               <!-- End case study box -->
            </div>
            <!-- End col -->
         </div>
         <!-- End Row -->
      </div>
      <!-- Container End -->
   </section>
   <!-- TW case end -->

   <section id="tw-testimonial" class="tw-testimonial bg-overlay">
      <div class="container">
         <div class="row">
            <div class="col text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay=".5s">
               <div class="section-heading">
                  <h2 class="text-white">
                     <small>Client’s Love</small>
                     Love from <span>Client</span>
                  </h2>
               </div>
            </div>
            <!-- COl End -->
         </div>
         <!-- Row End -->
         <div class="row justify-content-center">
            <div class="col-md-10 text-center">
               <div class="testimonial-slider owl-carousel">
                  <div class="testimonial-content">
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with customers, and increase sales nascetur ridiculus mus. </p>
                        <i class="icon icon-quote2"></i>
                     </div>
                     <!-- Testimonial text end -->
                     <div class="testimonial-image">
                        <img src="<?php echo base_url('assets/images/testimonial/profile.jpg');?>" alt="">
                     </div>
                     <div class="testimonial-meta">
                        <h4>
                           Jason Stattham
                           <small>CEO Microhost</small>
                        </h4>
                     </div>
                     <!-- Testimonial Meta end -->
                  </div>
                  <div class="testimonial-content">
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with customers, and increase sales nascetur ridiculus mus. </p>
                        <i class="icon icon-quote2"></i>
                     </div>
                     <!-- Testimonial text end -->
                     <div class="testimonial-image">
                        <img src="<?php echo base_url('assets/images/testimonial/profile.jpg');?>" alt="">
                     </div>
                     <div class="testimonial-meta">
                        <h4>
                           Jason Stattham
                           <small>CEO Microhost</small>
                        </h4>
                     </div>
                     <!-- Testimonial Meta end -->
                  </div>
               </div>
               <!-- Carousel End -->
            </div>
            <!-- COl End -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Testimonial end -->

   <section id="tw-pricing" class="tw-pricing">
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading wow fadeInDown" data-wow-duration="1s" data-wow-delay=".5s">
               <h2>
                  <small>Our Pricing</small>
                  Our Pricing <span>Table</span>
               </h2>
            </div>
            <!-- End Col -->
         </div>
         <!-- End Section Heading Row -->
         <div class="row">
            <div class="col-md-12 col-lg-4 wow zoomIn" data-wow-delay=".5s" data-wow-duration="1s">
               <div class="tw-price-box bg-orange">
                  <div class="pricing-feaures">
                     <h3 class="text-white">Starter Plan</h3>
                     <div class="pricing-price">
                        <sup>$</sup>
                        <strong>49.99</strong>
                        <small>/mo</small>
                     </div>
                     <!-- Pricing End -->
                     <ul>
                        <li>25 Analytics Compaign</li>
                        <li>1,300 Keywords</li>
                        <li>25 social media reviews</li>
                        <li>1 Free Optimization</li>
                        <li>24/7 support</li>
                     </ul>
                  </div>
                  <!-- Pricing Features End -->
                  <a href="#" class="btn btn-white tw-mt-30">buy now</a>
               </div>
               <!--  pricing box ends -->
            </div>
            <!-- COl end -->
            <div class="col-md-12 col-lg-4 wow zoomIn" data-wow-delay="1.2s" data-wow-duration="1s">
               <div class="tw-price-box bg-yellow">
                  <div class="pricing-feaures">
                     <h3 class="text-white">Basic Plan</h3>
                     <div class="pricing-price">
                        <sup>$</sup>
                        <strong>69.99</strong>
                        <small>/mo</small>
                     </div>
                     <!-- Pricing End -->
                     <ul>
                        <li>25 Analytics Compaign</li>
                        <li>1,300 Keywords</li>
                        <li>25 social media reviews</li>
                        <li>1 Free Optimization</li>
                        <li>24/7 support</li>
                     </ul>
                  </div>
                  <!-- Pricing Features End -->
                  <a href="#" class="btn btn-white tw-mt-30">buy now</a>
               </div>
               <!--  pricing box ends -->
            </div>
            <!-- COl end -->
            <div class="col-md-12 col-lg-4 wow zoomIn" data-wow-delay="1.6s" data-wow-duration="1s">
               <div class="tw-price-box bg-green">
                  <div class="pricing-feaures">
                     <h3 class="text-white">Advanced Plan</h3>
                     <div class="pricing-price">
                        <sup>$</sup>
                        <strong>99.99</strong>
                        <small>/mo</small>
                     </div>
                     <!-- Pricing End -->
                     <ul>
                        <li>25 Analytics Compaign</li>
                        <li>1,300 Keywords</li>
                        <li>25 social media reviews</li>
                        <li>1 Free Optimization</li>
                        <li>24/7 support</li>
                     </ul>
                  </div>
                  <!-- Pricing Features End -->
                  <a href="#" class="btn btn-white tw-mt-30">buy now</a>
               </div>
               <!--  pricing box ends -->
            </div>
            <!-- COl end -->
         </div>
      </div>
      <!-- Container End -->
   </section>
   <!-- End Pricing -->

   <section id="tw-blog" class="tw-blog">
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading wow fadeInDown" data-wow-duration="1s" data-wow-delay=".5s">
               <h2>
                  <small>Latest News</small>
                  Our Latest <span>News</span>
               </h2>
            </div>
            <!-- Col end -->
         </div>
         <!-- Row End -->
         <div class="row wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">
            <div class="col-lg-4 col-md-12">
               <div class="tw-latest-post">
                  <div class="latest-post-media text-center">
                     <img src="<?php echo base_url('assets/images/news/post1.jpg');?>" alt="blog_image_one" class="img-fluid">
                  </div>
                  <!-- End Latest Post Media -->
                  <div class="post-body">
                     <div class="post-item-date">
                        <div class="post-date">
                           <span class="date">29</span>
                           <span class="month">May</span>
                        </div>
                     </div>
                     <!-- End Post Item Date -->
                     <div class="post-info">
                        <div class="post-meta">
                           <span class="post-author">
                              Posted by <a href="#">Admin</a>
                           </span>
                        </div>
                        <!-- End Post Meta -->
                        <h3 class="post-title"><a href="#">SEO trend to look for the best in 2018</a></h3>
                        <div class="entry-content">
                           <p>
                              One of the top 100 advertising of a marketing agencies know that how to grow your busines
                           </p>
                        </div>
                        <!-- End Entry Content -->
                        <a href="#" class="tw-readmore">Read More
                           <i class="fa fa-angle-right"></i>
                        </a>
                     </div>
                     <!-- End Post info -->
                  </div>
                  <!-- End Post Body -->
               </div>
               <!-- End Tw Latest Post -->
            </div>
            <!-- End Col -->
            <div class="col-lg-4 col-md-12">
               <div class="tw-latest-post">
                  <div class="latest-post-media text-center">
                     <img src="<?php echo base_url('assets/images/news/post2.jpg');?>" alt="blog_image_one" class="img-fluid">
                  </div>
                  <!-- End Latest Post Media -->
                  <div class="post-body">
                     <div class="post-item-date">
                        <div class="post-date">
                           <span class="date">29</span>
                           <span class="month">May</span>
                        </div>
                     </div>
                     <!-- End Post Item Date -->
                     <div class="post-info">
                        <div class="post-meta">
                           <span class="post-author">
                              Posted by <a href="#">Admin</a>
                           </span>
                        </div>
                        <!-- End Post Meta -->
                        <h3 class="post-title"><a href="#">SEO trend to look for the best in 2018</a></h3>
                        <div class="entry-content">
                           <p>
                              One of the top 100 advertising of a marketing agencies know that how to grow your busines
                           </p>
                        </div>
                        <!-- End Entry Content -->
                        <a href="#" class="tw-readmore">Read More
                           <i class="fa fa-angle-right"></i>
                        </a>
                     </div>
                     <!-- End Post info -->
                  </div>
                  <!-- End Post Body -->
               </div>
               <!-- End Tw Latest Post -->
            </div>
            <!-- End Col -->
            <div class="col-lg-4 col-md-12">
               <div class="tw-latest-post">
                  <div class="latest-post-media text-center">
                     <img src="<?php echo base_url('assets/images/news/post3.jpg');?>" alt="blog_image_one" class="img-fluid">
                  </div>
                  <!-- End Latest Post Media -->
                  <div class="post-body">
                     <div class="post-item-date">
                        <div class="post-date">
                           <span class="date">29</span>
                           <span class="month">May</span>
                        </div>
                     </div>
                     <!-- End Post Item Date -->
                     <div class="post-info">
                        <div class="post-meta">
                           <span class="post-author">
                              Posted by <a href="#">Admin</a>
                           </span>
                        </div>
                        <!-- End Post Meta -->
                        <h3 class="post-title"><a href="#">SEO trend to look for the best in 2018</a></h3>
                        <div class="entry-content">
                           <p>
                              One of the top 100 advertising of a marketing agencies know that how to grow your busines
                           </p>
                        </div>
                        <!-- End Entry Content -->
                        <a href="#" class="tw-readmore">Read More
                           <i class="fa fa-angle-right"></i>
                        </a>
                     </div>
                     <!-- End Post info -->
                  </div>
                  <!-- End Post Body -->
               </div>
               <!-- End Tw Latest Post -->
            </div>
            <!-- End Col -->

            <div class="col-md-12 text-center tw-mt-60">
               <a href="#" class="btn btn-primary">view all</a>
            </div>
         </div>
         <!-- End Row -->
      </div>
      <!-- Container End -->
   </section>
   <!-- End tw blog -->

   <section id="tw-client" class="tw-client">
      <div class="container">
         <div class="row  wow fadeInUp">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client1.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client2.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client3.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client4.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client5.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client2.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client3.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client4.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/clients/client5.png');?>" alt="">
                     </div>
                     <!-- End Clients logo -->
                  </div>
                  <!-- End Client wrapper -->
               </div>
               <!-- End Carousel -->
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
   <!-- End Tw Client -->
<!DOCTYPE html>

<html lang="en" class="default-style">

<head>
  <title>Login v1 - Pages - Appwork</title>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <link rel="icon" type="image/x-icon" href="favicon.ico">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

  <!-- Icon fonts -->
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/fonts/fontawesome.css') ?>">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/fonts/ionicons.css') ?>">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/fonts/linearicons.css') ?>">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/fonts/open-iconic.css') ?>">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/fonts/pe-icon-7-stroke.css') ?>">

  <!-- Core stylesheets -->
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/css/rtl/bootstrap.css') ?>" class="theme-settings-bootstrap-css">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/css/rtl/appwork.css') ?>" class="theme-settings-appwork-css">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/css/rtl/theme-corporate.css') ?>" class="theme-settings-theme-css">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/css/rtl/colors.css') ?>" class="theme-settings-colors-css">
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/css/rtl/uikit.css') ?>">
  <link rel="stylesheet" href="<?php echo site_url('assets/css/demo.css') ?>">

  <script src="<?php echo site_url('assets/vendor/js/material-ripple.js') ?>"></script>
  <script src="<?php echo site_url('assets/vendor/js/layout-helpers.js') ?>"></script>

  <!-- Core scripts -->
  <script src="<?php echo site_url('assets/vendor/js/pace.js') ?>"></script>
  <script src="<?php echo site_url('assets/js/jquery-3.2.1.min.js') ?>"></script>

  <!-- Libs -->
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') ?>">
  <!-- Page -->
  <link rel="stylesheet" href="<?php echo site_url('assets/vendor/css/pages/authentication.css') ?>">
</head>

<body>
  <div class="page-loader">
    <div class="bg-primary"></div>
  </div>

  <!-- Content -->

  <div class="authentication-wrapper authentication-1 px-4">
    <div class="authentication-inner py-5">

      <!-- Logo -->
      <div class="d-flex justify-content-center align-items-center">
        <div class="ui-w-60">
          <div class="w-100 position-relative" style="padding-bottom: 54%">
            <!-- logo -->
          </div>
        </div>
      </div>
      <!-- / Logo -->

      <!-- Form -->
      <form class="my-5" method="POST" action="<?php echo site_url('login/do_login'); ?>">
        <div class="form-group">
          <label class="form-label">Email</label>
          <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
          <label class="form-label d-flex justify-content-between align-items-end">
            <div>Password</div>
          </label>
          <input type="password" class="form-control" name="password">
        </div>
        <div class="d-flex justify-content-between align-items-center m-0">
          <label class="custom-control custom-checkbox m-0">

          </label>
          <button type="submit" class="btn btn-primary">Sign In</button>
        </div>
      </form>
      <!-- / Form -->

    </div>
  </div>

  <!-- / Content -->

  <!-- Core scripts -->
  <script src="<?php echo site_url('assets/vendor/libs/popper/popper.js') ?>"></script>
  <script src="<?php echo site_url('assets/vendor/js/bootstrap.js') ?>"></script>
  <script src="<?php echo site_url('assets/vendor/js/sidenav.js') ?>"></script>

  <!-- Libs -->
  <script src="<?php echo site_url('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') ?>"></script>

  <!-- Demo -->
  <script src="<?php echo site_url('assets/js/demo.js') ?>"></script>

</body>

</html>
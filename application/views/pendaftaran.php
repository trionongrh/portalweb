   <div id="banner-area" class="banner-area bg-green">
      <div class="container">
         <div class="row">
            <div class="col-lg-4 col-md-6 ml-auto">
               <div class="banner-heading">
                  <h1 class="banner-title">Pendaftaran</h1>
                  <h4>Sudah mendaftar ?</h4>
                  <h5><a href="<?php echo site_url('login') ?>" class="badge badge-warning">Masuk sekarang</a></h5>
               </div>
            </div>
            <!-- Col end -->
         </div>
         <!-- Row end -->
      </div>
      <!-- Container end -->
   </div>
   <!-- Banner area end -->

   <section id="main-container" class="main-container">

      <div class="container">
         <div class="row">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     Form Pendaftaran <span>Karya</span>
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 ml-auto mr-auto"></span>
               </div>
               <!-- End Heading -->
            </div>
            <!-- End Col -->
         </div>
         <!-- End Title Row -->
         <div class="contact-us-form">
            <form id="contact-form" class="contact-form" action="<?php echo site_url('pendaftaran/submit') ?>" method="POST">
               <div class="error-container"></div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group">
                        <?php 
                           echo form_input(array('class' => 'form-control form-nisn', 'name' => 'nisn_pendaftar', 'placeholder' => 'NISN', ));
                        ?>
                     </div>
                  </div>
                  <!-- Col end -->
                  <div class="col-lg-6">
                     <div class="form-group">
                        <?php 
                           echo form_input(array('class' => 'form-control form-nokip', 'name' => 'no_kip_pendaftar', 'placeholder' => 'No KIP ( Kartu Indonesia Pintar )', 'type' => 'text'));
                        ?>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group">
                        <?php 
                           echo form_input(array('class' => 'form-control form-nama', 'name' => 'nama_pendaftar', 'placeholder' => 'Nama Lengkap', 'type' => 'text'));
                        ?>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group">
                        <?php 
                           echo form_input(array('class' => 'form-control form-email', 'name' => 'email_pendaftar', 'placeholder' => 'Email', 'type' => 'email'));
                        ?>
                     </div>
                  </div>
                  <div class="col-lg-3">
                     <div class="form-group">
                        <?php 
                           echo form_input(array('class' => 'form-control form-tempat-lahir', 'name' => 'tempat_lahir', 'placeholder' => 'Tempat Tanggal Lahir', 'type' => 'text'));
                        ?>
                     </div>
                  </div>
                  <div class="col-lg-3">
                     <div class="form-group">
                        <?php 
                           echo form_input(array('class' => 'form-control form-tanggal-lahir', 'name' => 'tanggal_lahir', 'placeholder' => 'Tanggal Lahir', 'type' => 'date', 'value'=> date('Y-m-d')));
                        ?>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group">
                        <?php 
                           echo form_input(array('class' => 'form-control form-kontak', 'name' => 'kontak_pendaftar', 'placeholder' => 'No Telp / HP', 'type' => 'text'));
                        ?>
                     </div>
                  </div>
                  <div class="col-lg-12">
                     <div class="form-group">
                        <textarea class="form-control form-alamat" id="alamat_pendaftar" placeholder="Alamat" rows="3" name="alamat_pendaftar"></textarea>
                     </div>
                  </div>
                  <!-- Col 12 end -->
               </div>
               <!-- Form row end -->
               <div class="text-center">
                  <button class="btn btn-primary tw-mt-40" type="submit">Daftar</button>
               </div>
            </form>
            <!-- Form end -->
         </div>
         <!-- Contact us form end -->
         <!-- End Row -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Main container end -->

   <script type="text/javascript">
      $('#contact-form').submit(function () {

         var $form = $(this),
            $error = $form.find('.error-container'),
            action = $form.attr('action');
         
         $error.slideUp(750, function () {
            $error.hide();

            var 
               $nisn = $form.find('.form-nisn'),
               $nokip = $form.find('.form-nokip'),
               $nama = $form.find('.form-nama'),
               $email = $form.find('.form-email'),
               $tempat_lahir = $form.find('.form-tempat-lahir'),
               $tanggal_lahir = $form.find('.form-tanggal-lahir'),
               $kontak = $form.find('.form-kontak'),
               $alamat = $form.find('.form-alamat');

            $.post(action, {
                  nisn_pendaftar    : $nisn.val(),
                  no_kip_pendaftar  : $nokip.val(),
                  nama_pendaftar    : $nama.val(),
                  email_pendaftar   : $email.val(),
                  tempat_lahir      : $tempat_lahir.val(),
                  tanggal_lahir     : $tanggal_lahir.val(),
                  kontak_pendaftar  : $kontak.val(),
                  alamat_pendaftar  : $alamat.val(),
               },
               function (data) {
                  $error.html(data);
                  $error.slideDown('slow');
                  console.log(data);
                  if (data.match('berhasil') != null) {
                     $nisn.val('');
                     $nokip.val('');
                     $nama.val('');
                     $email.val('');
                     $tempat_lahir.val('');
                     $tanggal_lahir.val('<?php echo date('Y-m-d'); ?>');
                     $kontak.val('');
                     $alamat.val('');
                  }
                  $error.fadeTo(3000, 750).slideUp(750, function(){
                     $error.slideUp(500);
                  });
               }
            );
           
         });
         
         return false;

      });
   </script>
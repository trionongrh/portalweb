   <footer id="tw-footer" class="tw-footer">
      <div class="container">
         <div class="footer-contact">
            <div class="row">
               <div class="col-lg-4">
                  <ul class="footer-contact-info">
                     <li>
                        <span><i class="icon icon-map-marker2"></i></span>
                        <div class="info-wrapper">
                           <p class="info-title">Find us</p>
                           <p class="info-subtitle">1010 Avenue, NY 90001, USA</p>
                        </div>
                     </li>
                     <!-- li End -->
                  </ul>
                  <!-- Footer Info End -->
               </div>
               <!-- Col End -->
               <div class="col-lg-4">
                  <ul class="footer-contact-info">
                     <li>
                        <span><i class="icon icon-phone3"></i></span>
                        <div class="info-wrapper">
                           <p class="info-title">Call us</p>
                           <p class="info-subtitle">989-554-6987</p>
                        </div>
                     </li>
                     <!-- li End -->
                  </ul>
                  <!-- Footer Info End -->
               </div>
               <!-- Col End -->
               <div class="col-lg-4">
                  <ul class="footer-contact-info">
                     <li>
                        <span><i class="icon icon-envelope"></i></span>
                        <div class="info-wrapper">
                           <p class="info-title">Mail us</p>
                           <p class="info-subtitle">mail@example.com</p>
                        </div>
                     </li>
                     <!-- li End -->
                  </ul>
                  <!-- Footer Info End -->
               </div>
               <!-- Col End -->
            </div>
         </div>
         <div class="footer-content">
            <div class="footer-pattern">
               <img src="<?php echo base_url('assets/images/footer_map.png');?>" alt="">
            </div>
            <div class="row">
               <div class="col-md-12 col-lg-4">
                  <div class="tw-footer-info-box">
                     <a href="index-2.html" class="footer-logo">
                        <img src="<?php echo base_url('assets/images/footer_logo.png');?>" alt="footer_logo" class="img-fluid">
                     </a>
                     <p class="footer-info-text">
                        Lorem ipsum dolor sit amet, consec tetur adipisicing elit, sed do eiusmod tempor incididunt ut
                     </p>
                     <div class="footer-social-link">
                        <h3>Follow us</h3>
                        <ul>
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                           <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                           <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                           <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                     </div>
                     <!-- End Social link -->
                  </div>
                  <!-- End Footer info -->
               </div>
               <!-- End Col -->

               <div class="col-md-12 col-lg-4">
                  <div class="footer-widget">
                     <div class="section-heading">
                        <h3>Useful Links</h3>
                     </div>
                     <ul>
                        <li><a href="#">Search Engine</a></li>
                        <li><a href="#">Data analysis</a></li>
                        <li><a href="#">Disital marketing</a></li>
                        <li><a href="#">Web Analytics</a></li>
                        <li><a href="#">Social Marketing</a></li>
                     </ul>
                     <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Our Services</a></li>
                        <li><a href="#">Expert Team</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Latest News</a></li>
                     </ul>
                  </div>
                  <!-- End Footer Widget -->
               </div>
               <!-- End col -->
               <div class="col-md-12 col-lg-4">
                  <div class="footer-widget">
                     <div class="section-heading">
                        <h3>Subscribe</h3>
                     </div>
                     <p>Don’t miss to subscribe to our new feeds, kindly fill the form below.</p>
                     <form action="#">
                        <div class="form-row">
                           <div class="col tw-footer-form">
                              <input type="email" class="form-control" placeholder="Email Address">
                              <button type="submit"><i class="fa fa-send"></i></button>
                           </div>
                        </div>
                     </form>
                     <!-- End form -->
                  </div>
                  <!-- End footer widget -->
               </div>
               <!-- End Col -->
            </div>
            <!-- End Widget Row -->
         </div>
      </div>
      <!-- End Contact Container -->


      <div class="copyright">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-md-12">
                  <span>Copyright &copy; 2018, All Right Reserved SeoTime</span>
               </div>
               <!-- End Col -->
               <div class="col-lg-6 col-md-12">
                  <div class="copyright-menu">
                     <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Contact</a></li>
                     </ul>
                  </div>
               </div>
               <!-- End col -->
            </div>
            <!-- End Row -->
         </div>
         <!-- End Copyright Container -->
      </div>
      <!-- End Copyright -->
      <!-- Back to top -->
      <div id="back-to-top" class="back-to-top">
         <button class="btn btn-dark" title="Back to Top">
            <i class="fa fa-angle-up"></i>
         </button>
      </div>
      <!-- End Back to top -->
   </footer>
   <!-- End Footer -->

   <!-- Javascripts File
      =============================================================================-->

   <!-- Popper JS -->
   <script src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
   <!-- Bootstrap jQuery -->
   <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
   <!-- Owl Carousel -->
   <script src="<?php echo base_url('assets/js/owl-carousel.2.3.0.min.js');?>"></script>
   <!-- Waypoint -->
   <script src="<?php echo base_url('assets/js/waypoints.min.js');?>"></script>
   <!-- Counter Up -->
   <script src="<?php echo base_url('assets/js/jquery.counterup.min.js');?>"></script>
   <!-- Video Popup -->
   <script src="<?php echo base_url('assets/js/jquery.magnific.popup.js');?>"></script>
   <!-- Smooth scroll -->
   <script src="<?php echo base_url('assets/js/smoothscroll.js');?>"></script>
   <!-- WoW js -->
   <script src="<?php echo base_url('assets/js/wow.min.js');?>"></script>
   <!-- Template Custom -->
   <script src="<?php echo base_url('assets/js/main.js');?>"></script>
</body>


<!-- Mirrored from demo.themewinter.com/html/seotime/multipage/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 21:36:42 GMT -->
</html>
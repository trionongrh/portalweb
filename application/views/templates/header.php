<!doctype html>
<html lang="en">


<!-- Mirrored from demo.themewinter.com/html/seotime/multipage/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 21:35:22 GMT -->
<head>
   <!-- Basic Page Needs =====================================-->
   <meta charset="utf-8">

   <!-- Mobile Specific Metas ================================-->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <!-- Site Title- -->
   <title><?php echo (isset($page_title)) ? ucwords($page_title) : ''; ?> - Portal Pendaftaran Karya</title>


   <!-- CSS
   ==================================================== -->
   <!--Font Awesome -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" />

   <!-- Animate CSS -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css'); ?>">

   <!-- Iconic Fonts -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/icofonts.css'); ?>" />

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">

   <!-- Owl Carousel -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/owlcarousel.min.css'); ?>" />

   <!-- Video Popup -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/magnific-popup.css'); ?>" />

   <!--Style CSS -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">

   <!--Responsive CSS -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css'); ?>">

   
   <!-- initialize jQuery Library -->
   <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>


   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->


</head>

<body>
   <!-- header======================-->
   <header>
      <div class="tw-head">
         <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
               <a class="navbar-brand tw-nav-brand" href="<?php echo site_url(); ?>">
                  <img src="<?php echo base_url('assets/images/logo/logo.png'); ?>" alt="seotime">
               </a>
               <!-- End of Navbar Brand -->
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                  aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
               </button>
               <!-- End of Navbar toggler -->
               <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                  <ul class="navbar-nav">
                     <li class="nav-item <?php echo ($page_title=='beranda') ? 'active' : ''; ?>">
                        <a class="nav-link" href="<?php echo site_url('beranda'); ?>">
                           Beranda
                        </a>
                     </li>
                     <li class="nav-item <?php echo ($page_title=='info lomba') ? 'active' : ''; ?>">
                        <a class="nav-link" href="#">
                           Info Lomba
                        </a>
                     </li>
                     <li class="nav-item <?php echo ($page_title=='petunjuk') ? 'active' : ''; ?>">
                        <a class="nav-link" href="#">
                           Petunjuk
                        </a>
                     </li>
                     <li class="nav-item <?php echo ($page_title=='faq') ? 'active' : ''; ?>">
                        <a class="nav-link" href="#">
                           FAQ
                        </a>
                     </li>
                     <li class="nav-item <?php echo ($page_title=='pendaftaran') ? 'active' : ''; ?>">
                        <a class="nav-link" href="<?php echo site_url('pendaftaran'); ?>">
                           <span class="badge badge-warning" style="font-size: 13px;">DAFTAR SEKARANG!</span>
                        </a>
                     </li>
                  </ul>
                  <!-- End Navbar Nav -->
                  <!-- <div class="tw-off-search d-none d-lg-inline-block">
                     <div class="tw-search">
                        <i class="icon icon-search"></i>
                     </div>
                  </div> -->
                  <!-- End off canvas menu -->
               </div>
               <!-- End of navbar collapse -->
            </nav>
            <!-- End of Nav -->
         </div>
         <!-- End of Container -->
      </div>
      <!-- End tw head -->
   </header>
   <!-- End of Header area -->